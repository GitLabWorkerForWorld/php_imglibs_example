<?php
    // run an edge detection filter on an image
    // $image = new imagick("./images/1.png");
    // $matrix1 = [
    //     [-1, -1, -1],
    //     [ 0,  0,  0],
    //     [ 1,  1,  1],
    // ];
    // $kernel1 = ImagickKernel::fromMatrix($matrix1);
    // // $edgeArray = array(-1,-1,-1,-1,8,-1,-1,-1,-1);
    // // $image->convolveImage (array(1,1));
    // $image->convolveImage ($kernel1);
    // $image->thresholdImage(1);
    // header('Content-type: image/png');
    // echo $image;
    function addKernel($imagePath) {
        $matrix1 = [
            [-1, -1, -1],
            [ 0,  0,  0],
            [ 1,  1,  1],
        ];

        $matrix2 = [
            [-1,  0,  1],
            [-1,  0,  1],
            [-1,  0,  1],
        ];

        $kernel1 = ImagickKernel::fromMatrix($matrix1);
        $kernel2 = ImagickKernel::fromMatrix($matrix2);
        $kernel1->addKernel($kernel2);

        $imagick = new \Imagick(realpath($imagePath));
        $imagick->convolveImage($kernel1);
        header("Content-Type: image/png");
        echo $imagick->getImageBlob();

    }
    addKernel('./images/1.png');